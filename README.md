**Desarrolla aplicaciones web con base de datos**
**Alexander Palazuelos Beltran**
**5AVP**

Practica #1 - Alexander Palazuelos Beltran - 02/09/2022 - Practica Ejemplo
commit: d8da5445cc872842309a5e5ec5506c877efc7a38
https://gitlab.com/Alexander.37/desarrolla_aplicaciones_web/-/blob/main/Parcial%201/Ejemplo.html

Practica #2 - Alexander Palazuelos Beltran - 09/09/2022 - Practica Javascript
commit: d8da5445cc872842309a5e5ec5506c877efc7a38
https://gitlab.com/Alexander.37/desarrolla_aplicaciones_web/-/blob/main/Parcial%201/PracticaJavascript.html

Practica #3 - Alexander Palazuelos Beltran - 15/09/2022 - Practica web con base de datos
commit: c8a05859b3f6ba259c7868055a04df4170b2a0ab
https://gitlab.com/Alexander.37/desarrolla_aplicaciones_web/-/blob/main/Parcial%201/index.rar

Practica #4 - Alexander Palazuelos Beltran - 19/09/2022 - Consultar Datos
commit:7f30fbfb220871eadec53b4a969415b9b2c87f03
https://gitlab.com/Alexander.37/desarrolla_aplicaciones_web/-/blob/main/Parcial%201/consultarDatos.php

Practica #5 - Alexander Palazuelos Beltran -22/09/2022 - Registro de datos
commit:d70eb8b54fad8a168d61a3ffee7a48cd07aa523a
https://gitlab.com/Alexander.37/desarrolla_aplicaciones_web/-/blob/main/Parcial%201/Practica5.rar

Practica #6 - Alexander Palazuelos Beltran -26/09/2022 - Conexion
commit:0ef049e20dd8b9eb3c7aa314641441bd893d13b5
https://gitlab.com/Alexander.37/desarrolla_aplicaciones_web/-/blob/main/Parcial%201/Practica__6_Conexion.rar
